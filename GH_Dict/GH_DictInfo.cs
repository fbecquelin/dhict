﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace Dhictionary
{
    public class DhictInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "Dhictionary";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                return Properties.Resources.GHDict_Create;
            }
        }
        public override string Description
        {
            get
            {
                return "Dictionary type in Grasshopper.";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("0e3276a6-1c15-47e3-b227-e64e3b9e1861");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "Frédéric Becquelin";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "f.becquelin@protonmail.com";
            }
        }
    }
}
